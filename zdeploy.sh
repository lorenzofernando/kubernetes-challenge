#!/bin/bash
minikube start
eval $(minikube docker-env)

# We use a sudo-less `docker` invocation to use the minikube docker daemon instead of the host's 
docker build . -t node-sample

# The following three commands are identical to `minikube addons enable ingress`
# They are copied from `https://github.com/kubernetes/minikube/tree/master/deploy/addons/ingress`
# to allow the repo to be self-contained. 
kubectl apply -f ./deploy/minikube-ingress-plugin/ingress-configmap.yaml
kubectl apply -f ./deploy/minikube-ingress-plugin/ingress-dp.yaml
kubectl apply -f ./deploy/minikube-ingress-plugin/ingress-svc.yaml

# Configure the application
kubectl apply -f ./deploy/configmap.yaml
kubectl apply -f ./deploy/ingress.yaml
kubectl apply -f ./deploy/deployment.yaml
kubectl apply -f ./deploy/service.yaml
