# Kubernetes challenge

Deploy this application to [Minikube](https://github.com/kubernetes/minikube) and customise the environment variable to display your name.

```
$ curl $(minikube ip)
Hello Dan!
```

## Instructions

- Fork this repo
- Build the Docker image
- Write yaml files for a deployment, service, ingress and configmap
- Deploy your application to Minikube
- You should be able to `curl` Minikube's ip and retrieve the string `Hello {yourname}!`
- Commit your files to Github

## Notes

There's no need to push the Docker image to a Docker registry. You should be able to build and use the image from within Minikube.

You can expose Minikube's Docker daemon with:

```shell
$ eval (minkube docker-env)
```

## Solution
My solution is found in `./zdeploy.sh`. After startup, it takes a while for the ingress controller to register the sample node endpoint, so I would give it a few moments before running `curl $(minikube ip)`. This demo was done with `minikube` version `v0.28.2`, running Kubernetes version `1.10.0`, managed with `kubectl` version `1.10.1`.